# Sparkpost metrics exporter for prometheus

Aplication gather metrics from sparkpost via API and convert them to prometheus format. Exporter has been writen in GOlang.


## Running application

Configure **API_TOKEN** environment variable and run binary
```bash
export API_TOKEN=Ya.......76
sparkpost-exporter -by-sender -o metrics_file.prom

```


Api token for sparkpost could be set via cli parameters. To print all configuration variables use parameter -h
```bash
Usage of sparkpost-exporter:
  -by-sender
        Generate metrics with sender domain tag
  -o string
        Output file for metrics (default "sparkpost_metrics.prom")
  -token string
        Sparkpost API_TOKEN, could be also set via env variable. (default "XXXXX")
  -url string
        Sparkpost API URL (default "https://api.sparkpost.com")

```

## Using in production

Run the binary from cron like this (please don't use root's crontab)

```bash

#m h dom mon dow    command
05 * * * *          ~/bin/sparkpost-exporter -token SECRET_TOKEN -o /tmp/metrics/sparkpost_exporter.prom &> /dev/null
```


**In future releases shoud be available http listening exporter.**


## Build application

```bash
# Build everything
make all

# Print build options
make help
```