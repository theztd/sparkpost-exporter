package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
)

func HandleIndex(w http.ResponseWriter, req *http.Request) {
	fmt.Fprint(w, "Go to /metrics path for prometheus metrics")
}

func server() {
	listen := flag.String("listen", "0.0.0.0", "Listen address")
	port := flag.Int("port", 9125, "Exporter will listen on this port")

	flag.Parse()

	log.Println()
	log.Println()
	log.Printf("Starting sparkpost metrics exporter for prometheus... (VERSION: %s)", VERSION)
	log.Println("============================================================================")
	log.Println()
	log.Println()
	log.Println("Listen address:        ", *listen)
	log.Println("Listen port:           ", *port)
	// log.Println("Sparkpost URL:         ", *sparkpost_url)
	// log.Println("Sparkpost API TOKEN:   ", API_TOKEN[:3], ". . . . . . . . . . .", API_TOKEN[len(API_TOKEN)-3:])
	log.Println()
	log.Println()

	http.HandleFunc("/", HandleIndex)

	http.ListenAndServe(":9090", nil)

}
