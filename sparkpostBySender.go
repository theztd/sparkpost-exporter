package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/fatih/structs"
)

type deliverabilityMetricsBySender struct {
	Count_injected                 int    `json:"count_injected"`
	Count_accepted                 int    `json:"count_accepted"`
	Count_admin_bounce             int    `json:"count_admin_bounce"`
	Count_block_bounce             int    `json:"count_block_bounce"`
	Count_bounce                   int    `json:"count_bounce"`
	Count_clicked                  int    `json:"count_clicked"`
	Count_delayed                  int    `json:"count_delayed"`
	Count_delayed_first            int    `json:"count_delayed_first"`
	Count_delivered                int    `json:"count_delivered"`
	Count_delivered_first          int    `json:"count_delivered_first"`
	Count_delivered_subsequent     int    `json:"count_delivered_subsequent"`
	Count_generation_failed        int    `json:"count_generation_failed"`
	Count_generation_rejection     int    `json:"count_generation_rejection"`
	Count_hard_bounce              int    `json:"count_hard_bounce"`
	Count_inband_bounce            int    `json:"count_inband_bounce"`
	Count_initial_rendered         int    `json:"count_initial_rendered"`
	Count_outofband_bounce         int    `json:"count_outofband_bounce"`
	Count_policy_rejection         int    `json:"count_policy_rejection"`
	Count_rejected                 int    `json:"count_rejected"`
	Count_rendered                 int    `json:"count_rendered"`
	Count_sent                     int    `json:"count_sent"`
	Count_soft_bounce              int    `json:"count_soft_bounce"`
	Count_spam_complaint           int    `json:"count_spam_complaint"`
	Count_targeted                 int    `json:"count_targeted"`
	Count_undetermined_bounce      int    `json:"count_undetermined_bounce"`
	Count_unique_clicked           int    `json:"count_unique_clicked"`
	Count_unique_confirmed_opened  int    `json:"count_unique_confirmed_opened"`
	Count_unique_initial_rendered  int    `json:"count_unique_initial_rendered"`
	Count_unique_rendered          int    `json:"count_unique_rendered"`
	Total_delivery_time_first      int    `json:"total_delivery_time_first"`
	Total_delivery_time_subsequent int    `json:"total_delivery_time_subsequent"`
	Total_msg_volume               int    `json:"total_msg_volume"`
	Sending_domain                 string `json:"sending_domain"`
}

type resultBySender struct {
	Metrics []deliverabilityMetricsBySender `json:"results"`
}

func GetDeliverabilityMetricsBySender() {

	t := time.Now().Add(-1 * time.Hour)
	last_hour := fmt.Sprintf("%d-%02d-%02dT%02d", t.Year(), int(t.Month()), t.Day(), t.Hour())
	api_endpoint := fmt.Sprintf("/api/v1/metrics/deliverability/sending-domain?from=%s:00&to=%s:59&metrics=%s&timezone=Europe/Prague&limit=50&order_by=count_injected", last_hour, last_hour, metrics_type)

	log.Println("Scrapling metrics for time ", last_hour)

	client := http.Client{}
	req, err := http.NewRequest("GET", sparkpost_url+api_endpoint, nil)
	if err != nil {
		log.Panic(err)
	}

	req.Header = http.Header{
		"Content-Type":  []string{"application/json"},
		"Authorization": []string{sparkpost_token},
	}

	res, err := client.Do(req)
	if err != nil {
		log.Panic(err)
	}

	r_body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Panic(err)
	}

	metrics := resultBySender{}
	jsErr := json.Unmarshal(r_body, &metrics)
	if jsErr != nil {
		log.Panic(jsErr)
	}

	// Open file for writing metrics
	f, err := os.Create(metrics_file)
	if err != nil {
		log.Panic(err)
	}
	defer f.Close()

	f.WriteString("# HELP sparkpost_metrics_by_sending_domain Counted by sending status and sender_domain\n")
	f.WriteString("# TYPE sparkpost_metrics_by_sending_domain gauge\n")
	// convert struct to iterable map by package "github.com/fatih/structs"
	for mId := range metrics.Metrics {
		sending_domain := metrics.Metrics[mId].Sending_domain
		metrics_map := structs.Map(metrics.Metrics[mId])
		log.Println("Generating:", mId, sending_domain)

		for k, v := range metrics_map {
			if k != "Sending_domain" {
				f.WriteString(fmt.Sprintf("sparkpost_metrics_by_sending_domain{status=\"%s\", sending_domain=\"%s\"} %d\n", strings.ToLower(k), sending_domain, v))
			}
		}
	}

}
