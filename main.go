package main

import (
	"flag"
	"os"
)

func GetENV(name string, fallback string) string {
	// Try to get ENV variable if it does not exists use fallback string
	_ret := os.Getenv(name)
	if _ret != "" {
		return _ret
	}

	return fallback
}

var (
	sparkpost_url, sparkpost_token, metrics_file string
)

func main() {

	flag.StringVar(&sparkpost_url, "url", "https://api.sparkpost.com", "Sparkpost API URL")
	flag.StringVar(&sparkpost_token, "token", GetENV("API_TOKEN", ""), "Sparkpost API_TOKEN, could be also set via env variable.")
	flag.StringVar(&metrics_file, "o", "sparkpost_metrics.prom", "Output file for metrics")
	detail := flag.Bool("by-sender", false, "Generate metrics with sender domain tag")

	flag.Parse()

	if *detail == true {
		GetDeliverabilityMetricsBySender()
	} else {
		GetDeliverabilityMetrics()
	}

}
